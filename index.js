import express from "express";
import dotnev from "dotenv";
import routes from "./src/routes/index.js";

dotnev.config();

const app = express();

app.use(express.json());

app.use("/api", routes);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Serveur en cours d'exécution sur le port ${PORT}`);
});

export default app;
