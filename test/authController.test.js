import { expect, use } from "chai";
import chaiHttp from "chai-http";
import { describe, it, beforeEach } from "mocha";
import app from "../index.js";

// chai.use(chaiHttp);
const server = use(chaiHttp);

describe("AuthController", () => {
  let accessToken; // Déclare la variable accessToken ici

  beforeEach(async () => {
    // Connecte l'utilisateur avant chaque test
    const res = await server
      .request(app)
      .post("/api/auth/login")
      .send({ username: "utilisateur", password: "motdepasse" });
    accessToken = res.body.accessToken;
  });

  describe("POST /api/auth/login", () => {
    it("devrait retourner un jeton JWT valide lors de la connexion réussie", (done) => {
      server
        .request(app)
        .post("/api/auth/login")
        .send({ username: "utilisateur", password: "motdepasse" })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.have.property("accessToken");
          done();
        });
    });

    it("devrait retourner une erreur 401 en cas d'informations d'identification incorrectes", (done) => {
      server
        .request(app)
        .post("/api/auth/login")
        .send({ username: "utilisateur", password: "mauvaismotdepasse" })
        .end((err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("POST /api/auth/register", () => {
    it("devrait créer un nouvel utilisateur avec succès", (done) => {
      const username = "nouvel_utilisateur_" + Date.now(); // Utilise un nom d'utilisateur unique
      server
        .request(app)
        .post("/api/auth/register")
        .send({ username: username, password: "nouveaumotdepasse" })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.text).to.equal("Utilisateur enregistré avec succès");
          done();
        });
    });

    it("devrait retourner une erreur 400 si le nom d'utilisateur existe déjà", (done) => {
      server
        .request(app)
        .post("/api/auth/register")
        .send({ username: "utilisateur", password: "motdepasse" })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("POST /api/auth/logout", () => {
    it("devrait retourner un message de déconnexion réussie", (done) => {
      server
        .request(app)
        .post("/api/auth/logout")
        .set("Authorization", `Bearer ${accessToken}`) // Utilise accessToken ici
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.text).to.equal("Déconnexion réussie");
          done();
        });
    });

    // it("devrait retourner une erreur 400 si aucun jeton n'est fourni", (done) => {
    //   server
    //     .request(app)
    //     .post("/api/auth/logout")
    //     .end((err, res) => {
    //       expect(res).to.have.status(400);
    //       done();
    //     });
    // });
  });
});
