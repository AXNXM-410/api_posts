import jwt from "jsonwebtoken";
import { promisify } from "util";

// Middleware pour protéger les routes nécessitant une authentification
export async function protect(req, res, next) {
  // Récupère le token JWT depuis les en-têtes
  const token = req.headers.authorization?.split(" ")[1];

  if (!token) {
    return res.status(401).json({ message: "Vous n'êtes pas authentifié" });
  }

  try {
    // Vérifie si le token est valide
    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    req.user = decoded; // Ajoute les données de l'utilisateur au req pour une utilisation ultérieure
    next();
  } catch (error) {
    return res.status(401).json({ message: "Token JWT invalide" });
  }
}
