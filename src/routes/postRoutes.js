import express from "express";
import { createPost, getAllPosts } from "../controllers/PostController.js";
import { protect } from "../middleware/authMiddleware.js";

const router = express.Router();

router.post("/posts", protect, createPost);
router.get("/posts", getAllPosts);

export default router;
