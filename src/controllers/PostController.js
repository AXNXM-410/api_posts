import Post from "../models/Post.js";

// Controller pour créer un nouveau post
export async function createPost(req, res, next) {
  const { title, content } = req.body;
  const userId = req.user.id; // Récupère l'ID de l'utilisateur depuis le token JWT

  try {
    const post = await Post.createPost(title, content, userId);
    res.status(201).json(post);
  } catch (error) {
    next(error);
  }
}

// Controller pour récupérer tous les posts
export async function getAllPosts(req, res, next) {
  try {
    const posts = await Post.getAllPosts();
    res.json(posts);
  } catch (error) {
    next(error);
  }
}
