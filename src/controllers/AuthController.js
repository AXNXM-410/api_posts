import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import User from "../models/User.js";

let blacklist = [];

class AuthController {
  static async login(req, res) {
    const { username, password } = req.body;

    try {
      const user = await User.findByUsername(username);

      if (!user) {
        return res.status(401).send("Nom d'utilisateur ou mot de passe incorrect");
      }

      const validPassword = await bcrypt.compare(password, user.password);

      if (!validPassword) {
        return res.status(401).send("Nom d'utilisateur ou mot de passe incorrect");
      }

      const accessToken = jwt.sign(
        { id: user.id, username: user.username },
        process.env.JWT_SECRET
      );
      res.json({ accessToken });
    } catch (err) {
      console.error("Erreur lors de la connexion de l'utilisateur", err);
      res.status(500).send("Erreur lors de la connexion de l'utilisateur");
    }
  }

  static async register(req, res) {
    const { username, password } = req.body;

    try {
      const existingUser = await User.findByUsername(username);

      if (existingUser) {
        return res.status(400).send("Le nom d'utilisateur existe déjà");
      }

      const hashedPassword = await bcrypt.hash(password, 10);
      const newUser = await User.create({ username, password: hashedPassword });
      res.send("Utilisateur enregistré avec succès");
    } catch (err) {
      console.error("Erreur lors de l'enregistrement de l'utilisateur", err);
      res.status(500).send("Erreur lors de l'enregistrement de l'utilisateur");
    }
  }

  static async logout(req, res) {
    const token = req.headers["authorization"]?.split(" ")[1];

    if (!token) {
      return res.sendStatus(400);
    }

    blacklist.push(token);
    res.send("Déconnexion réussie");
  }

  // Vérifie si un jeton est valide et n'est pas dans la liste noire
  static verifyToken(token) {
    if (!token) {
      return false;
    }

    return !blacklist.includes(token);
  }
}

export default AuthController;
