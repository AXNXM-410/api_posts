import db from "../database/db.js";

class User {
  static async create({ username, password }) {
    const query = "INSERT INTO users (username, password) VALUES ($1, $2) RETURNING *";
    const values = [username, password];
    const result = await db.query(query, values);
    return result.rows[0];
  }

  static async findByUsername(username) {
    const query = "SELECT * FROM users WHERE username = $1";
    const values = [username];
    const result = await db.query(query, values);
    return result.rows[0];
  }
}

export default User;
