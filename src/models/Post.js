import db from "../database/db.js";

class Post {
  // Méthode pour créer un nouveau post
  static async createPost(title, content, userId) {
    try {
      const result = await db.query(
        "INSERT INTO posts (title, content, user_id) VALUES ($1, $2, $3) RETURNING *",
        [title, content, userId]
      );
      return result.rows[0];
    } catch (error) {
      throw new Error("Erreur lors de la création du post");
    }
  }

  // Méthode pour récupérer tous les posts
  static async getAllPosts() {
    try {
      const result = await db.query("SELECT * FROM posts");
      return result.rows;
    } catch (error) {
      throw new Error("Erreur lors de la récupération des posts");
    }
  }
}

export default Post;
